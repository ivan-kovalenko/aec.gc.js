var gulp = require("gulp");

var Server = require("karma").Server;

var requireDir = require("require-dir");
requireDir("./gulp-tasks");

gulp.task("run-tests", function (done) {
    new Server
        (
            {
                configFile: __dirname + "/karma.conf.js",
                singleRun: true
            },
            function () { done(); }
        )
        .start();
});


gulp.task("build",
    [
        "gulp-jshint",
        "browserify-main"
    ]);

gulp.task("default", function ()
{
    gulp.watch("./source/**/*.js", ["gulp-jshint", "browserify-main"]);
});


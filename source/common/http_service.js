"use strict";

var axios = require("axios");

module.exports = function HttpService()
{
    var self = this;

    self.get = function(url, parameters)
    {
        return axios.get(url, { params: parameters });
    };
};
"use strict";
//about: entry point to form scripts
//exports: exports form scripts to global scope


//dependencies
require('es6-promise').polyfill();

var _httpService = require("./common/http_service.js");
var _pageService = require("./crm/page_service.js");
var _areasService = require("./entities/areas/areas_service.js");
var _regionsService = require("./entities/regions/regions_service.js");
var _officesService = require("./entities/offices/offices_service.js");

var _jobSetupForm = require("./entities/job-setup/job-setup_form.js");
var _jobSetupFormService = require("./entities/job-setup/job-setup_form-service.js");

var _opportunityForm = require("./entities/opportunity/opportunity_form.js");
var _opportunityFormService = require("./entities/opportunity/opportunity_form-service.js");


//initialization
var AEC = window.AEC || {};
var httpService = new _httpService();
var pageService = new _pageService();

var regionsService = new _regionsService(httpService, pageService);
var areasService = new _areasService(httpService, pageService, regionsService);
var officesService = new _officesService(httpService, pageService);

var jobSetupFormService = new _jobSetupFormService(areasService, pageService);
var opportunityFormService = new _opportunityFormService(areasService, pageService, officesService);


AEC.JobSetupForm = new _jobSetupForm(jobSetupFormService);
AEC.OpportunityForm = new _opportunityForm(opportunityFormService);

window.AEC = AEC;
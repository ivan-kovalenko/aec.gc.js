"use strict";

module.exports = function ()
{
    var self = this;
    var page = window.Xrm.Page;

    self.getLookupValue = function (fieldName)
    {
        var attributeValue = page.getAttribute(fieldName).getValue();

        if (!attributeValue) { return null;}

        return attributeValue[0];
    };
    self.setLookupValue = function (fieldName, lookupItem)
    {
        var lookup = [lookupItem];

        page.getAttribute(fieldName).setValue(lookup);
    };
    self.save = function ()
    {
        if (page.data.entity.getIsDirty())
        {
            page.data.save();
        }
    };

    self.getEntityOdataUrl = function (entitySetName)
    {
        var baseUrl = page.context.getClientUrl();
        var apiUrl = "/api/data/v8.0/";

        return baseUrl + apiUrl + entitySetName;
    };

    
};
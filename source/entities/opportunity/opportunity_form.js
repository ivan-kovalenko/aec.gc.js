"use strict";

module.exports = function OpportunityForm(opportunityFormService)
{
    var self = this;

    self.onSave = function ()
    {
        var region = opportunityFormService.getRegion();
        var group = opportunityFormService.getGroup();
        var office = opportunityFormService.getOffice();
        
        //if office field is emty, set it to default office
        if (!office)
        {
            opportunityFormService.setDefaultOffice();
        }
        //if region and group already present, then finish the execution;
        if (region && group) { return; }
        
        
        opportunityFormService.getArea()        
            .then(function (result)
            {
                if (!result) { return; }
                
                if (!region && result.region)
                {
                    opportunityFormService.setRegion(result.region);
                }
                if (!group && result.group)
                {
                    opportunityFormService.setGroup(result.group);
                }
            })
            .catch(function (error) { console.log(error); });
    };
};
"use strict";
//about: service encapsulates buisnes logic with Area entity.


module.exports = function AreasService(http, pageService)
{
    var self = this;
    var _entityType = "gc_area";
    var _groupEntityType = "gc_group";
    var _regionEntityType = "gc_region";
    var _entitySetName = "gc_areas";
    

    self.getById = function (areaId)
    {        
        var entityUrlPart = _entitySetName + "(" + areaId + ")"; 

        var params =
            {
                $expand: "gc_Region"
            }; 
        var url = pageService.getEntityOdataUrl(entityUrlPart);    
        
        return http.get(url, params)
            .then(function (result)
            {
                if (result.data)
                {
                    return mapResult(result.data);
                }
                return null;
            });
    };

    function mapResult(result)
    {
        var area =
            {
                id: result.gc_areaid,
                name: result.gc_name,
                entityType: _entityType,
                
            };
        
        if (!result.gc_Region) { return area; }
        
        area.region =
            {
                id: result.gc_Region.gc_regionid,
                name: result.gc_Region.gc_name,
                entityType: _regionEntityType
            };
        area.group =
            {
                id: result.gc_Region._gc_group_value,
                name: "",
                entityType: _groupEntityType
            };
        return area;
    }
};

//list of areas with releated entity names
    // var _areas =
    //     [
    //         {
    //             name: "Central",
    //             regionName: "Central",
    //             groupName: "Large Projects"
    //         },
    //         {
    //             name: "Northeast",
    //             regionName: "Northeast",
    //             groupName: "Large Projects"
    //         },
    //         {
    //             name: "Southeast",
    //             regionName: "Southeast",
    //             groupName: "Large Projects"
    //         },
    //         {
    //             name: "West",
    //             regionName: "West",
    //             groupName: "Large Projects"
    //         },
    //         {
    //             name: "Federal",
    //             regionName: "Federal",
    //             groupName: "Federal"
    //         },
    //         {
    //             name: "Kenny Power",
    //             regionName: "Kenny Power",
    //             groupName: "Kenny"
    //         },
    //         {
    //             name: "Kenny Tunnel",
    //             regionName: "Kenny Tunnel",
    //             groupName: "Kenny"
    //         },
    //         {
    //             name: "Kenny Civil",
    //             regionName: "Kenny Civil CM Build",
    //             groupName: "Kenny"
    //         },
    //         {
    //             name: "Kenny CM",
    //             regionName: "Kenny Civil CM Build",
    //             groupName: "Kenny"
    //         },
    //         {
    //             name: "Western Slope",
    //             regionName: "Kenny Underground",
    //             groupName: "Kenny"
    //         },
    //         {
    //             name: "Kenny Underground",
    //             regionName: "Kenny Underground",
    //             groupName: "Kenny"
    //         },
    //         {
    //             name: "Alaska",
    //             regionName: "Alaska",
    //             groupName: "Northwest"
    //         },
    //         {
    //             name: "Arizona",
    //             regionName: "Arizona",
    //             groupName: "Northwest"
    //         },
    //         {
    //             name: "Intermountain Central",
    //             regionName: "Intermountain Slurry Seal",
    //             groupName: "Northwest"
    //         },
    //         {
    //             name: "Intermountain Western",
    //             regionName: "Intermountain Slurry Seal",
    //             groupName: "Northwest"
    //         },
    //         {
    //             name: "Reno",
    //             regionName: "Nevada",
    //             groupName: "Northwest"
    //         },
    //         {
    //             name: "Las Vegas",
    //             regionName: "Nevada",
    //             groupName: "Northwest"
    //         },
    //         {
    //             name: "Utah Construction",
    //             regionName: "Utah",
    //             groupName: "Northwest"
    //         },
    //         {
    //             name: "Western WA",
    //             regionName: "Washington",
    //             groupName: "Northwest"
    //         },
    //         {
    //             name: "Eastern WA",
    //             regionName: "Washington",
    //             groupName: "Northwest"
    //         },
    //         {
    //             name: "Desert Cities",
    //             regionName: "Desert Cities",
    //             groupName: "Califirnia"
    //         },
    //         {
    //             name: "Sacramento",
    //             regionName: "Valley CA",
    //             groupName: "Califirnia"
    //         },
    //         {
    //             name: "Fresno",
    //             regionName: "Valley CA",
    //             groupName: "Califirnia"
    //         },
    //         {
    //             name: "Bakersfield",
    //             regionName: "Central CA",
    //             groupName: "Califirnia"
    //         },
    //         {
    //             name: "Palmdale Ventura",
    //             regionName: "Central CA",
    //             groupName: "Califirnia"
    //         },
    //         {
    //             name: "Santa Barbara",
    //             regionName: "Central CA",
    //             groupName: "Califirnia"
    //         },
    //         {
    //             name: "Coastal",
    //             regionName: "Coastal CA",
    //             groupName: "Califirnia"
    //         },
    //         {
    //             name: "San Diego",
    //             regionName: "San Diego",
    //             groupName: "Califirnia"
    //         },
    //     ];
    
    //finds Area by name
    // self.getByName = function (areaName)
    // {
    //     var areas = _areas.filter(function (x) { return x.name === areaName; });
    //     return areas[0];
    // };
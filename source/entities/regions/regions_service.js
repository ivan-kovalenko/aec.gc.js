"use strict";

module.exports = function RegionsService(http, pageService)
{
    var self = this;
    var _entityType = "gc_region";
    var _entitySetName = "gc_regions";

    self.getByName = function (regionName)
    {
        var url = pageService.getEntityOdataUrl(_entitySetName);
        var params =
            {
                $select: "gc_regionid,gc_name",
                $filter: "gc_name eq '" + regionName + "'"
            };
        
        return http.get(url, params)
            .then(function (result)
            {
                var mapped = mapResult(result);
                if (mapped.length > 0)
                {
                    return mapped[0];
                }

                return null;
            });
    };
    self.getById = function (regionId)
    {
        var entityUrlPart = _entitySetName + "(" + regionId + ")";
        
        var url = pageService.getEntityOdataUrl(entityUrlPart);
        
        return http.get(url)
            .then(function (result)
            {
                if (result.data)
                {
                    return result.data;
                }
                return null;
            });
    };

    function mapResult(result)
    {
        if (!result.data || !result.data.value) { return []; }
        
        var mapped = result.data.value.map(function (x)
        {
            return {
                id: x.gc_regionid,
                name: x.gc_name,
                entityType: _entityType
            };
        });

        return mapped;
    }
};
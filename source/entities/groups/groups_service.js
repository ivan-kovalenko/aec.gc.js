"use strict";
//service encaplsulates buisnes logic with Group entity.

module.exports = function GroupsService(http, pageService)
{
    var self = this;
    var _entityType = "gc_group";
    var _entitySetName = "gc_groups";

    //finds group by name    
    self.getByName = function (groupName)
    {
        var url = pageService.getEntityOdataUrl(_entitySetName);
        var params =
            {
                $select: "gc_groupid,gc_name",
                $filter: "gc_name eq '" + groupName + "'"
            };
        
        return http.get(url, params)
            .then(function (result)
            {
                var mapped = mapResult(result);
                if (mapped.length > 0)
                {
                    return mapped[0];
                }

                return null;
            });
    };

    //fetch result mapping    
    function mapResult(result)
    {
        if (!result.data || !result.data.value) { return []; }
        
        var mapped = result.data.value.map(function (x)
        {
            return {
                id: x.gc_groupid,
                name: x.gc_name,
                entityType: _entityType
            };
        });

        return mapped;
    }
};
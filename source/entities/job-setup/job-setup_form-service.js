"use strict";
var Promise = require("es6-promise").Promise;

module.exports = function JobSetupFormService(areasService, pageService)
{
    var self = this;
    var _areaFieldName = "gc_area";
    var _groupFieldName = "gc_group";
    var _regionFieldName = "gc_region";

    self.getArea = function ()
    {
        var area = pageService.getLookupValue(_areaFieldName);
        if (!area)
        {
            return Promise.reject("area not found");
        }  

        var areaId = area.id.substring(1, area.id.length - 1).toLowerCase();
        return areasService.getById(areaId);
    };

    self.getRegion = function ()
    {        
        return pageService.getLookupValue(_regionFieldName);
    };
    self.setRegion = function (region)
    {
        if (!region)
        {
            return;
        }
        pageService.setLookupValue(_regionFieldName, region);
        pageService.save();
    };
    
    self.getGroup = function ()
    {
        return pageService.getLookupValue(_groupFieldName); 
    };
    self.setGroup = function (group)
    {
        if (!group)
        {
            return;
        }
        pageService.setLookupValue(_groupFieldName, group);
        pageService.save();
    };
};
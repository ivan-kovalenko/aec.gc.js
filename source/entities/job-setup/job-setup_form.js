"use strict";

module.exports = function JobSetupForm(jobSetupFormService)
{
    var self = this;

    self.onSave = function ()
    {
        
        var region = jobSetupFormService.getRegion();
        var group = jobSetupFormService.getGroup();
        
        //if region and group already present, then finish the execution;
        if (region && group) { return; }
        
        jobSetupFormService.getArea()        
            .then(function (result)
            {
                if (!result) { return; }
                
                if (!region && result.region)
                {
                    jobSetupFormService.setRegion(result.region);
                }
                if (!group && result.group)
                {
                    jobSetupFormService.setGroup(result.group);
                }
            })
            .catch(function (error) { console.log(error); });
    };
};
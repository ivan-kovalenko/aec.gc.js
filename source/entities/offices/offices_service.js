"use strict";

module.exports = function OfficesService(http, pageService)
{
    var self = this;
    var _entityType = "gc_office";
    var _entitySetName = "gc_offices";

    self.getByCode = function (officeCode)
    {        
        var url = pageService.getEntityOdataUrl(_entitySetName);
        var parameters =
            {
                $filter: "gc_code eq '" + officeCode + "'"
            };
        
        return http.get(url, parameters)
            .then(function (result)
            {
                if (result.data && result.data.value.length > 0)
                {
                    return mapResult(result.data.value[0]);
                }
                return null;
            });
    };

    function mapResult(entity)
    {
        return {
            id: entity.gc_officeid,
            name: entity.gc_name,
            entityType: _entityType
        };
    
    }
};
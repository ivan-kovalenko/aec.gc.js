var gulp = require("gulp");
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');

gulp.task("browserify-main", function () {
    var bundleName = "index.js";
    var externalLibs = [];
    var bundler = browserify(
        {
            entries: "./source/main_module.js",
            debug: true
        })
        .external(externalLibs)

    return bundler
        .bundle()
        .pipe(source(bundleName))
        .pipe(buffer())
        //.pipe(sourcemaps.init({loadMaps: true}))
        //.pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("./build"));
});
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');


gulp.task("gulp-jshint", function ()
{
    var options =
        {
            node: true,
            jasmine: true,
            globals:
            {
                "inject": false,
                "window": false
            }
        };
    return gulp.src("./source/**/*.js")
        .pipe(jshint(options))
        .pipe(jshint.reporter(stylish));
});
